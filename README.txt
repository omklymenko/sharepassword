START APPLICATION:
- unzip devchallengeTask
- navigate into devchallengeTask
- run docker-compose up --build (sometimes issues occur on rerun application inside Docker on Windows)

HOW IT WORKS:
- provide password to receive link and share
- open link to decode password 

THINGS TO IMPROVE:
- probably NoSQL is better for this case