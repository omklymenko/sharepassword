package it.devchallenge.sharepassword.dao;

import it.devchallenge.sharepassword.entity.Password;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository
@EnableAutoConfiguration
public class PasswordDao {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Save new password into DB
     *
     * @param  password
     * @return link
     */
    public String savePassword(Password password) {
        entityManager.persist(password);
        entityManager.flush();
        return password.getLink();
    }

    /**
     * Get Password by link
     *
     * @param  link
     * @return Password
     */
    public Password getPasswordByLink(String link) {
        TypedQuery<Password> query = entityManager.createQuery(
                "select pass from Password pass where pass.link = :link",
                Password.class);
        query.setParameter("link", link);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
