package it.devchallenge.sharepassword.controller;

import it.devchallenge.sharepassword.service.PasswordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.security.GeneralSecurityException;

@RestController
public class PasswordController {

    private PasswordService passwordService;

    private static final Logger logger = LoggerFactory.getLogger(PasswordController.class);

    @Autowired
    public PasswordController(PasswordService passwordService) {
        this.passwordService = passwordService;
    }

    @RequestMapping("/")
    public String index() {
        return "Share password application";
    }

    @PostMapping(value = "/addPassword")
    public String addPassword(String originalPassword, Integer time) {
        logger.info("/addPassword request received");

        if(originalPassword == null) {
            return "Password not provided";
        }

        try {
            return "http://localhost:8080/getPassword/" + passwordService.create(originalPassword, time);
        } catch (Exception e) {
            logger.error("Error occurred while trying to process api request", e);
            return "Error occurred while trying to process api request";
        }
    }

    @RequestMapping("/getPassword/{link}")
    public @ResponseBody String getPassword(@PathVariable String link) throws GeneralSecurityException {
        logger.info("/getPassword request received");
        return passwordService.getPasswordByLink(link);
    }

}
