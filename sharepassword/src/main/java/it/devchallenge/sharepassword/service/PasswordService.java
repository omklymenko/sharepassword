package it.devchallenge.sharepassword.service;

import it.devchallenge.sharepassword.dao.PasswordDao;
import it.devchallenge.sharepassword.entity.Password;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
@Transactional
public class PasswordService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PasswordService.class);

    private PasswordDao passwordDao;
    private EncryptService encryptService;

    @Autowired
    public PasswordService(PasswordDao passwordDao, EncryptService encryptService) {
        this.passwordDao = passwordDao;
        this.encryptService = encryptService;
    }

    /**
     * Encode the password, save, return link to user
     *
     * @param originalPassword
     * @param time
     * @return link
     */
    public String create(String originalPassword, Integer time) throws GeneralSecurityException {
        Password password = new Password();
        List<String> result = encryptService.encodePassword(originalPassword);
        LOGGER.info(String.format("%s was successfully encoded", originalPassword));
        String link = result.get(1);
        password.setEncodedPassword(result.get(0));
        password.setLink(link);
        if(time != null) {
            password.setMaxTime(time);
        }
        passwordDao.savePassword(password);
        return link;
    }

    /**
     * Get decoded password by link if it's not expired and not used
     *
     * @param link
     * @return originalPassword
     */
    public String getPasswordByLink(String link) throws GeneralSecurityException {
        Password password = passwordDao.getPasswordByLink(link);
        if(!isExpired(password) && !password.getUsed()){
            password.setUsed(true);
            return encryptService.decodePassword(password.getEncodedPassword(), link);
        } else {
            return "Password expired";
        }
    }

    /**
     * Check expiration status
     *
     * @param password
     * @return Boolean
     */
    private Boolean isExpired(Password password) {
        LocalDateTime currentDateTime = LocalDateTime.now(ZoneId.of("GMT+03:00"));
        LocalDateTime tempDateTime = password.getLocalDateTime();
        long hours = tempDateTime.until( currentDateTime, ChronoUnit.HOURS);
        return hours > password.getMaxTime();
    }

}