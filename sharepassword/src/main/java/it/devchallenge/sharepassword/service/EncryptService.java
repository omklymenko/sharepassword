package it.devchallenge.sharepassword.service;

import it.devchallenge.sharepassword.dao.PasswordDao;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EncryptService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptService.class);

    private final PasswordDao passwordDao;

    private IvParameterSpec ivParameterSpec;
    private SecretKeySpec secretKeySpec;
    private Cipher cipher;

    @Autowired
    public EncryptService(PasswordDao passwordDao){
        this.passwordDao = passwordDao;
    }

    /**
     * Encode the password to save into DB
     *
     * @param  originalPassword
     * @return encodedPassword, link
     */
    List<String> encodePassword(String originalPassword) throws GeneralSecurityException {
        String ivParameter = RandomStringUtils.randomAlphanumeric(16);
        String secretKey = RandomStringUtils.randomAlphanumeric(16);
        ivParameterSpec = new IvParameterSpec(ivParameter.getBytes(StandardCharsets.UTF_8));
        secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "AES");
        cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(originalPassword.getBytes());
        List<String> result = new ArrayList<>();
        result.add(Base64.encodeBase64String(encrypted));
        result.add(ivParameter + "_" + secretKey);

        return result;
    }

    /**
     * Decode previously saved password
     *
     * @param encodedPassword
     * @param link
     * @return originalPassword
     */
    String decodePassword(String encodedPassword, String link) throws GeneralSecurityException {
        String ivParameterToDecrypt = link.split("_")[0];
        String secretKeyToDecrypt = link.split("_")[1];
        ivParameterSpec = new IvParameterSpec(ivParameterToDecrypt.getBytes(StandardCharsets.UTF_8));
        secretKeySpec = new SecretKeySpec(secretKeyToDecrypt.getBytes(StandardCharsets.UTF_8), "AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] decryptedBytes = cipher.doFinal(Base64.decodeBase64(encodedPassword));
        LOGGER.info("The password was successfully decoded");
        return new String(decryptedBytes);
    }

}
